from django.urls import path, include
from store_api import views
from rest_framework import routers

#router = routers.DefaultRouter()
#router.register("Users",views.UserViewSet)


urlpatterns = [
    path('hello-view/',views.HelloApi.as_view()),
    path('create-user/',views.UserAPI.as_view(),name="create_user"),
    path('list-user/',views.UserList.as_view(),name="list_user"),
    path('users/',views.UsersView.as_view(),name="users_api"),
    path('users/register_product',views.ProductView.as_view(),name="register_product"),
    path('users/list_product',views.ProductView.as_view(),name="list_product"),
    path('users/purchase_product',views.PurchaseView.as_view(),name="purchase_product_api"),
    path('users/paymentMethod',views.PaymentMethodView.as_view(),name="paymentMethod"),
    path('users/sale',views.SaleView.as_view(),name="sale"),
    path('users/sale_user/<pk>',views.SaleView_User.as_view(),name="sale_user"),
    
    
]
