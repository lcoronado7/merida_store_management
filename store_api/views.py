#from django.shortcuts import render
from django.shortcuts import render, redirect
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status,viewsets,generics
from store_api import serializers
from .models import User, Product, PaymentMethod, Purchase, Sale,UserProfileManager
from .serializers import HelloSerializer,UserSerializer, ProductSerializer, PaymentMethodSerializer, PurchaseSerializer, SaleSerializer
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, JsonResponse 
from django.http import Http404
from django.contrib.auth import authenticate
import requests
from django.http import HttpResponseRedirect
from django.urls import reverse


class HelloApi(APIView):
    """ API de  prueba"""
    serializer_class= HelloSerializer #Configura la APIview para tener el serializer clase creado


    def get(self,request,formart=None):
        """Retorna lista de APIViews"""
        an_apiview = [
            'Usando metodos HTTP como fundiones (GET,POST,PATH,PUT,DELETE)',
            'Similar a una vista tradicional de Django',
            'Nos da mayor control sobre la logica de la aplicacion',
            'Esta mapeado manualmente a los URLs'
        ]
        return  Response({'message':'hello','data':an_apiview}) #Transforma la informacion en formato JSON siendo el objecto una lista o diccionario


    #Para los POST/PUT y las acciones que requieran de inputs se utiliza un serializador
    #Para recibir los datos y transformarlos en formato JSON
    
    def post(self, request):
        """ Crea un mensaje con nuestro nombre"""
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            name = serializer.validated_data.get('name')

            message = f'hello {name}'
            return Response({'message': message})
        else:
            return Response(
                serializer.errors,
                status= status.HTTP_400_BAD_REQUEST
            )
    
    def put(self, request, pk=None):
        """ Actualizar un objecto """
        return Response({'method':'PUT'})

    def patch(self,request,pk=None):
        """Actualizacion parcial de un objecto, actualizar un atributo especifico """
        return Response({'method':'PATCH'})
    
    def delete(self,request,pk=None):
        """Borrar Objecto"""
        return Response({'method':'DELETE'})


class UserAPI(APIView):
    
    serializer_class= UserSerializer

    def post(self,request):
        serializer = self.serializer_class(data=request.data)
        if serializer. is_valid():
            user = serializer.save()
            return Response(serializer.data,status= status.HTTP_201_CREATED)
            
        else:
            return Response(serializer.errors,status= status.HTTP_400_BAD_REQUEST)

class UserList(generics.ListCreateAPIView):
    queryset= User.objects.all()
    serializer_class=UserSerializer

#Viewset se encargan de recibir los datos o consultar

class UserViewSet(viewsets.ModelViewSet):
    queryset= User.objects.all()
    serializer_class=UserSerializer

class UsersView(APIView):
    serializer_class= UserSerializer
    def post(self,request):
        
        print(str(request.data))
        type_view=request.data['type_view']

        if(type_view=="login"):
            email=request.data['email']
            password=request.data['password']
            user = authenticate(email=email, password=password)
            if user is not None:
                user=User.objects.get(email=email)
                admin=user.is_admin
                if(admin==True):
                    #return redirect("init_admin")
                    return  Response({'message':'autentificado','type_user':'admin','user':user.id},status= status.HTTP_200_OK) 
                elif(admin==False):
                    #return redirect("init_user")
                    return  Response({'message':'autentificado','type_user':'user','user':user.id},status= status.HTTP_200_OK) 
            else:
                return Response(serializer.errors,status= status.HTTP_400_BAD_REQUEST)
        elif(type_view=="register"):
            serializer = self.serializer_class(data=request.data)
            if serializer. is_valid():
                email=request.data['email']
                name=request.data['name']
                last_name=request.data['last_name']
                password=request.data['password']

                user=User.objects.create_user(email,name,last_name,password)
                return Response({'id_user':user.id,'email':user.email,'password':user.password},status= status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors,status= status.HTTP_400_BAD_REQUEST)

class ProductView(APIView):
    serializer_class= ProductSerializer
    def post(self,request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            name=request.data['name']
            price=request.data['price']
            stock_product=request.data['stock_product']
            description= request.data['description']
            product=Product(name=name,price=price,stock_product=stock_product,description=description)
            product.save()
            return redirect("init_admin")
    
    def get(self,request):
        
        objects_product=Product.objects.values_list()
        return  Response({'message':'get_product','data':objects_product})
    
    
class PurchaseView(APIView):
    serializer_class= PurchaseSerializer
    def post(self,request):
        serializer = self.serializer_class(data=request.data)
        
        if serializer.is_valid():
            id_product=request.data['id_product']
            
            try:
                product = Product.objects.get(id=id_product)
                stock_product= product.stock_product
            except Product.DoesNotExist:
                return Response({'detail':'product not found'}, status=status.HTTP_404_NOT_FOUND)
            quantity=request.data['quantity']
            description=request.data['description']
            purchase=Purchase(quantity=int(quantity),description=description)
            purchase.id_product=product
            purchase.save()
            """Actualizacion Stock"""
            stock_product= product.stock_product + int(quantity)
            product.stock_product=stock_product
            product.save()

            return  Response({'message':'post_purchase'})
        return Response(serializer.errors,status= status.HTTP_400_BAD_REQUEST)

    def get(self,request):
        purchases=Purchase.objects.all().values()
        print(str(purchases))
        purchases_object=[]
        contador=0
        purchases=list(purchases)
        for pur in purchases:
            product=Product.objects.get(id=pur['id_product_id'])
            purchase={
                "product_id": pur['id_product_id'],
                "product_name":product.name,
                "quantity":pur['quantity'],
                "description":pur['description']
            }
            purchases_object.insert(contador,purchase)
            contador=contador+1
        return  Response({'message':'get_product','data':purchases_object},status=status.HTTP_200_OK)

class PaymentMethodView(APIView):
    serializer_class= PaymentMethodSerializer
    def post(self,request):
        serializer = self.serializer_class(data=request.data)
        print(str(request))
        if serializer.is_valid():
            method_type=request.data['method_type']
            description=request.data['description']
            paymentMethod=PaymentMethod(method_type=method_type,description=description) 
            paymentMethod.save()
            return  Response({'message':'post_paymentMethod'})

        return Response(serializer.errors,status= status.HTTP_400_BAD_REQUEST)
    
    def get(self,request):
        paymentMethod=PaymentMethod.objects.values_list()
        return  Response({'message':'get_paymentMethod','data':paymentMethod}, status=status.HTTP_200_OK)


class SaleView(APIView):
    serializer_class= SaleSerializer
    def post(self,request):
        
        try:
            id_user=request.data['id_user']
            id_product=request.data['id_product']
            id_paymentMethod=request.data['id_paymentMethod']
            user = User.objects.get(id=id_user)
            product = Product.objects.get(id=id_product)
            paymentMethod = PaymentMethod.objects.get(id=id_paymentMethod)
            stock_product= product.stock_product
            quantity=request.data['quantity']
            IVA=request.data['IVA']
            subtotal=float(product.price) * int(quantity)
            total=float(subtotal)+(float(subtotal)*(float(IVA)/100))
            description=request.data['description']
            sale={
                "id_user":id_user,
                "id_product":id_product,
                "id_paymentMethod":id_paymentMethod,
                "quantity":quantity,
                "subtotal":subtotal,
                "total":total,
                "description":description,
            }
        except Product.DoesNotExist or PaymentMethod.DoesNotExist or User.DoesNotExist:
            return Response({'detail':'product not found'}, status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer_class(data=sale)
        print(str(request))
        if serializer.is_valid():
            
            sale=Sale(quantity=sale['quantity'],subtotal=sale['subtotal'],total=sale['total'],
                        description=sale['description'])
            sale.id_user=user
            sale.id_product=product
            sale.id_paymentMethod=paymentMethod
            
            """Actualizacion Stock"""
            stock_product= product.stock_product - int(quantity)
            if(stock_product>=0):
                sale.save()
                product.stock_product=stock_product
                product.save()

            return Response({'message':'get_sale'}, status=status.HTTP_200_OK)


                
    

        return Response(serializer.errors,status= status.HTTP_400_BAD_REQUEST)
    
    def get(self,request):
        sales=Sale.objects.all().values()
        print(str(sales))
        sales_object=[]
        contador=0
        sales=list(sales)
        for sal in sales:
            product=Product.objects.get(id=sal['id_product_id'])
            user=User.objects.get(id=sal['id_user_id'])
            sale={
                "product_id": sal['id_product_id'],
                "product_name":product.name,
                "name_usuer":user.name,
                "last_name_user":user.last_name,
                "quantity":sal['quantity'],
                "total":sal['total'],
                "description":sal['description']
            }
            sales_object.insert(contador,sale)
            contador=contador+1
        return  Response({'message':'get_product','data':sales_object},status=status.HTTP_200_OK)

class SaleView_User(APIView):
    serializer_class= SaleSerializer
    def get(self,request,pk):
        sales=Sale.objects.filter(id_user=pk).values()
        print(str(sales))
        sales_object=[]
        contador=0
        sales=list(sales)
        print(str(sales))
        for sal in sales:
            product=Product.objects.get(id=sal['id_product_id'])
            user=User.objects.get(id=sal['id_user_id'])
            sale={
                "product_id": sal['id_product_id'],
                "product_name":product.name,
                "name_usuer":user.name,
                "last_name_user":user.last_name,
                "quantity":sal['quantity'],
                "total":sal['total'],
                "description":sal['description']
            }
            sales_object.insert(contador,sale)
            contador=contador+1
        return  Response({'message':'get_product','data':sales_object},status=status.HTTP_200_OK)
        

        

      
