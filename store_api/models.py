from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import BaseUserManager


class UserProfileManager(BaseUserManager):
    """Manager para el manipular el usuario"""

    def create_user(self,email,name,last_name, password=None):
        """ Crear nuevo usuario """
        if not email:
            raise ValueError('Usuario requiere email') 
        
        email = self.normalize_email(email)
        user = self.model(email=email,name=name,last_name=last_name)
        
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_superuser(self,email,name, password,last_name):
        """ Crear nuevo administrador """
        user=self.create_user(email,name,password,last_name)

        user.is_superuser=True
        user.is_admin=True
        
        user.save(using=self._db)
        return user



class User(AbstractBaseUser):
    """ Modelo BD para Usuario """
    email=models.EmailField(max_length=255,unique=True)
    name=models.CharField(max_length=255)
    last_name=models.CharField(max_length=255)
    is_active=models.BooleanField(default=True)
    is_superuser=models.BooleanField(default=False)
    is_admin=models.BooleanField(default=False)

    objects= UserProfileManager()

    USERNAME_FIELD= 'email'
    REQUIRED_FIELDS= ['name,last_name']

    def get_full_name(self):
        """Obtener nombre completo del usuario"""
        return self.name

    def __str__(self):
        """Cadena con datos usuario"""
        return self.email
    
    def has_perm(self,perm,obj=None):
        return True
    
    def has_module_perms(self,app_label):
        return True


class Product(models.Model):
    name = models.CharField(max_length = 255)
    price = models.CharField(max_length = 255)
    stock_product = models.IntegerField()
    description = models.TextField(max_length = 500)
    
    def __str__(self):
        return "%s %s" % (self.name, self.stock_product)

class PaymentMethod(models.Model):
    method_type = models.CharField(max_length = 255)
    description = models.TextField(max_length = 500)
    
    def __str__(self):
        return "%s %s" % (self.method_type, self.description)

class Purchase(models.Model):
    id_product = models.ForeignKey(Product, on_delete=models.CASCADE)  
    quantity = models.IntegerField()
    description = models.TextField(max_length = 500)
    
    def __str__(self):
        return "%s %s" % (self.id_product, self.description)

class Sale(models.Model):
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)  
    id_product = models.ForeignKey(Product, on_delete=models.CASCADE)  
    id_paymentMethod = models.ForeignKey(PaymentMethod, on_delete=models.CASCADE)  
    quantity = models.IntegerField()
    subtotal= models.FloatField()
    total= models.FloatField()
    description = models.TextField(max_length = 500)
    
    def __str__(self):
        return "%s %s" % (self.id_product, self.description)