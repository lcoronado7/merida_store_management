from .models import User, Product, PaymentMethod, Purchase, Sale,UserProfileManager
from rest_framework import serializers

class HelloSerializer(serializers.Serializer):
    """Serializa un campo para probar la API View"""
    name= serializers.CharField(max_length=10)

class UserSerializer(serializers.ModelSerializer):
     
    class Meta:
        model = User
        # fields = '__all__'
        fields = ['email','name','last_name','password']
        read_only_fields = ['is_admin','id']

class ProductSerializer(serializers.ModelSerializer):
     
    class Meta:
        model = Product
        # fields = '__all__'
        fields = ['name','price','stock_product','description']
        read_only_fields = ['id']

class PaymentMethodSerializer(serializers.ModelSerializer):
     
    class Meta:
        model = PaymentMethod
        # fields = '__all__'
        fields = ['method_type','description']
        read_only_fields = ['id']


class PurchaseSerializer(serializers.ModelSerializer):
     
    class Meta:
        model = Purchase
        # fields = '__all__'
        fields = ['id_product','quantity','description']
        read_only_fields = ['id']

class SaleSerializer(serializers.ModelSerializer):
     
    class Meta:
        model = Sale
        # fields = '__all__'
        fields = ['id_user','id_product','id_paymentMethod','quantity','subtotal','total','description']
        read_only_fields = ['id']
