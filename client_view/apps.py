from django.apps import AppConfig


class ClientViewConfig(AppConfig):
    name = 'client_view'
