from django.urls import path, include
from client_view import views



urlpatterns = [
    path('',views.index , name='home'),
    path('login',views.login , name='login'),
    path('register',views.register , name='register'),
    path('init_view',views.init_view , name='init_view'),
    path('init_user',views.user_view , name='init_user'),
    path('init_admin',views.admin_view , name='init_admin'),
    path('product_register',views.product_register , name='product_register'),
    path('purchase_register',views.purchase_register , name='purchase_register'),
    path('list_product_user',views.list_product_user , name='list_product_user'),
    path('list_product_admin',views.list_product_admin , name='list_product_admin'),
    path('view_purchase_register/<pk>',views.view_purchase_register , name='view_purchase_register'),
    path('purchase_register',views.purchase_register , name='purchase_register'),
    path('view_paymentmethod_register',views.view_paymentmethod_register , name='view_paymentmethod_register'),
    path('paymentmethod_register',views.paymentmethod_register , name='paymentmethod_register'),
    path('view_sale_register/<pk>',views.view_sale_register , name='view_sale_register'),
    path('sale_register',views.sale_register , name='sale_register'),
    path('sale_list_admin',views.sale_list_admin , name='sale_list_admin'),
    path('sale_list_user',views.sale_list_user , name='sale_list_user'),
    path('purchase_list',views.purchase_list , name='purchase_list'),
    
    path('logout', views.logout, name='logout'),
]
