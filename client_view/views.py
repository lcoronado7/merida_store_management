from django.shortcuts import render

import requests

def index(request):
    
    return render(
        request,
        'index.html',
        {}
    )

def register(request):
    
    return render(
        request,
        'user_template/register_user.html',
        {}
    )

def login(request):
    
    return render(
        request,
        'user_template/login_user.html',
        {}
    )

def init_view(request):

    type_view=request.POST.get('type_view')

    if type_view == 'login':
        email=request.POST.get('email')
        password=request.POST.get('password')
        
        try:
            login={
                "email":email,
                "password":password,
                "type_view":type_view
            }
            
            response = requests.post('http://127.0.0.1:8000/api/users/',data=login)
            
            if response.status_code == 200:
                data=response.json()

                if(data['message']=="autentificado"):
                    print(str("autentificado"))
                    type_user=data['type_user']
                    user=data['user']

                    if type_user == 'user':
                        request.session["user_id"]=user
                        return render(
                            request,
                            'user_template/user_view.html',
                            {'user_id':user}
                        )

                    elif type_user == 'admin':
                        request.session["user_id"]=user
                        return render(
                            request,
                            'user_template/admin_view.html',
                            {'user_id':user}
                        )
            else:
                return render(
                    request,
                    'user_template/login_user.html',
                    {}
                )  



        except Exception as a:
            print("register error "+str(a))
            return render(
                request,
                'user_template/login_user.html',
                {}
            )  


    elif(type_view != 'register'):
        return render(
            request,
            'user_template/login_user.html',
            {}
        )

    

    if type_view == 'register':
        name=request.POST.get('name')
        last_name=request.POST.get('last_name')
        email=request.POST.get('email')
        password=request.POST.get('password')
        register={
            "name":name,
            "last_name":last_name,
            "email":email,
            "password":password,
            "type_view":type_view
        }
        
        response = requests.post('http://127.0.0.1:8000/api/users/',data=register)
        if response.status_code == 201:
            data=response.json()
            return render(
                request,
                'user_template/login_user.html',
                {'id_user':data['id_user']}
            )
        else:
            return render(
                request,
                'user_template/register_user.html',
                {}
            )
        
    
    elif(type_view != 'login'):
        return render(
            request,
            'user_template/register_user.html',
            {}
        )


def user_view(request):

    print(str(request.GET))

    return render(
        request,
        'user_template/user_view.html',
        {}
    )

def admin_view(request):
    print(str(request.POST))
    return render(
        request,
        'user_template/admin_view.html',
        {}
    )

def product_register(request):
    
    return render(
        request,
        'product_template/product_register.html',
        {}
    )

def purchase_register(request,pk):
    
    return render(
        request,
        'product_purchase/purchase_register.html',
        {}
    )



def list_product_user(request):
    
    response = requests.get('http://127.0.0.1:8000/api/users/list_product')

    if response.status_code == 200:
        data=response.json()
        data=data['data']
        print(str(data))


        
    return render(
        request,
        'product_template/list_product_user.html',
        {"products":data}
    )

def list_product_admin(request):
    
    response = requests.get('http://127.0.0.1:8000/api/users/list_product')

    if response.status_code == 200:
        data=response.json()
        data=data['data']
        print(str(data))


        
    return render(
        request,
        'product_template/list_product_admin.html',
        {"products":data}
    )

def view_purchase_register(request,pk):
    
       
    return render(
        request,
        'purchase_template/purchase_register.html',
        {"pk":pk}
    )

def purchase_register(request):
    
    id_product=request.POST.get('id_product')
    quantity=request.POST.get('quantity')
    description=request.POST.get('description')
    purchase={
        "id_product":id_product,
        "quantity":quantity,
        "description":description
    }
    response = requests.post('http://127.0.0.1:8000/api/users/purchase_product',data=purchase)

    
       
    return render(
        request,
        'user_template/admin_view.html',
        {}
    )


def view_paymentmethod_register(request):
    
       
    return render(
        request,
        'paymentmethod_template/paymentmethod_register.html',
        {}
    )

def paymentmethod_register(request):
    method_type=request.POST.get('method_type')
    description=request.POST.get('description')
    paymentmethod={
        "method_type":method_type,
        "description":description
    }
    response = requests.post('http://127.0.0.1:8000/api/users/paymentMethod',data=paymentmethod)
       
    return render(
        request,
        'user_template/admin_view.html',
        {}
    )

def view_sale_register(request,pk):
    
    response = requests.get('http://127.0.0.1:8000/api/users/paymentMethod')
    if response.status_code == 200:
        data=response.json()
        data=data['data']

    return render(
        request,
        'sale_template/sale_register.html',
        {"pk":pk, "user_id":request.session["user_id"], "data_paymentmethod":data}
    )

def sale_register(request):
    print(str(request.POST))
    id_user=request.POST.get('id_user')
    id_product=request.POST.get('id_product')
    id_paymentMethod=request.POST.get('id_paymentMethod')
    quantity=request.POST.get('quantity')
    IVA=request.POST.get('iva')
    description=request.POST.get('description')

    sale={
        "id_user":id_user,
        "id_product":id_product,
        "id_paymentMethod":id_paymentMethod,
        "quantity":quantity,
        "IVA":IVA,
        "description":description

    }

    response = requests.post('http://127.0.0.1:8000/api/users/sale',data=sale)

    return render(
        request,
        'user_template/user_view.html',
        {}
    )

def purchase_list(request):
    response = requests.get('http://127.0.0.1:8000/api/users/purchase_product')

    if response.status_code == 200:
        data=response.json()
        data=data['data']
        print(str(data))


    return render(
        request,
        'purchase_template/purchase_list.html',
        {"purchases":data}
    )

def sale_list_admin(request):
    response = requests.get('http://127.0.0.1:8000/api/users/sale')

    if response.status_code == 200:
        data=response.json()
        data=data['data']
        print(str(data))


    return render(
        request,
        'sale_template/sale_list_admin.html',
        {"sales":data}
    )
def sale_list_user(request):
    response = requests.get('http://127.0.0.1:8000/api/users/sale_user/'+str(request.session['user_id']))

    if response.status_code == 200:
        data=response.json()
        data=data['data']
        print(str(data))


    return render(
        request,
        'sale_template/sale_list_user.html',
        {"sales":data}
    )

def logout(request):
    
    try:
        del request.session['user_id']
    except KeyError:
        pass

    return render(
        request,
        'index.html',
        {}
    )
